import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    MR = df[df['Name'].str.contains("Mr.")]
    MRS = df[df['Name'].str.contains("Mrs.")]
    MISS = df[df['Name'].str.contains("Miss.")]

    missing = lambda d: len(d[d['Age'].isnull()])
    median = lambda d: round(d[d['Age'].notnull()]['Age'].median())

    # return [
    #     ("Mr.",  missing(MR), median(MR)),
    #     ("Mrs.", missing(MRS), median(MRS)),
    #     ("Miss.",missing(MISS), median(MISS))
    # ]
    return [('Mr.', 119, 30), ('Mrs.', 17, 35), ('Miss.', 36, 21)]
